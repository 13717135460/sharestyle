import './assets/style/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router' // 引入路由配置
import api from "@/api/index.js"
import 'amfe-flexible'
import ElementPlus from 'element-plus' // 引入elemennt+
import vue3PhotoPreview from 'vue3-photo-preview'
import 'vue3-photo-preview/dist/index.css'
import 'element-plus/dist/index.css' // 引入 element-plus样式

// import './assets/iconfont/iconfont.css' // 引入本地阿里字体图标

const app = createApp(App)

console.log('环境判断')
console.log(import.meta.env)

// 类似vue2 Vue.prototype.$api
app.config.globalProperties.$name = 'hello wxy'

app.config.globalProperties.$api = api

// 过滤器
app.config.globalProperties.$filters = {
    // 千分位分割
    thousands(val) {
        if (typeof (val) === 'number') {
            return val.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
        }
    },
    // 金额保留2位小数
    formatMoney(val) {
        return "￥" + val.toFixed(2)
    }
}

app.use(createPinia())
app.use(router)
app.use(ElementPlus)
app.use(vue3PhotoPreview)
app.mount('#app')
