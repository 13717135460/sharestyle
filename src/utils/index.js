// 是否登录
export function isLogin() {
    sessionStorage.getItem('userInfo') ? true : false
}

import { ElMessage } from 'element-plus'

// 是否闰年
export function isLeapYear(year) {
    if (getType(year) != 'number') return false
    return (year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)
}

// 验证手机号码
export function isPhone(s) {
    return /^1[0-9]{10}$/.test(s)
}

// 验证数字 & 金额
export function isNumber(s) {
    return /^([1-9]\d{0,9}|0)(\.\d{1,2})?$/.test(s)
}

// 数字验证
export function isCardNo(s) {
    return /^\d+$/.test(s)
}

// 银行卡验证
export function isBankNo(s) {
    return /^([1-9]{1})(\d{9}|\d{10}|\d{11}|\d{12}|\d{13}|\d{14}|\d{15}|\d{16}|\d{17}|\d{18}|\d{20}|\d{21})$/.test(s)
}

// 版权编号验证(字母+数字组合)
export function isNo(s) {
    return /^[a-zA-Z-\d]+$/.test(s)
}

// 图片格式
export function isImg(s) {
    return /^image\/(jpeg|png|jpg|webp|svg|gif)$/.test(s)
}

// 文件格式
export function isFile(s) {
    return /\.+(doc|excel|rar|zip|psd|pdf)$/.test(s)
}

// 自定义格式
export function isCustom(s) {
    return /\.+(doc|excel|rar|zip|jpg|jpeg|png|gif|webp|pdf)$/.test(s)
}

// 统一社会信用代码 (营业执照)
export function isLicense(s) {
    return /^[0-9A-HJ-NPQRTUWXY]{2}\d{6}[0-9A-HJ-NPQRTUWXY]{10}$/.test(s)
}

// 身份证
export function isIdCard(s) {
    return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(s)
}

// 身份证后6位
export function isIdCardLsat(s) {
    return /(^\d{5}(\d|X|x)$)/.test(s)
}

// URL地址
export function isURL(s) {
    return /(^hap | ^http[s])?:\/\/.*/.test(s)
}

// 邮箱验证
export function isEmail(s) {
    return /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((.[a-zA-Z0-9_-]{2,3}){1,2})$/.test(s)
}

// 验证邮编
export function isPostCode(s) {
    return /^(0[1-7]|1[0-356]|2[0-7]|3[0-6]|4[0-7]|5[1-7]|6[1-7]|7[0-5]|8[013-6])\d{4}$/g.test(s)
}

// 特殊字符判断
export function isPatrn(s) {
    return /[`~!@#$%^&*()_|+<>?"{}.\/;'[\]]/im.test(s)
}

// 数组分组 fn -> 为了分组依据，如：item => item.parentId，数组将依据parentId分组
export function groupBy(list, fn) {
    const groups = {}
    if (getType(list) !== 'array') return {}
    list.forEach(item => {
        const group = JSON.stringify(fn(item))
        groups[group] = groups[group] || []
        groups[group].push(item)
    })
    return groups
}

// 防抖
export function debounce(func, wait) {
    let timer;
    return function () {
        let args = arguments;
        if (timer) clearTimeout(timer);
        timer = setTimeout(() => {
            func.apply(this, args);
        }, wait)
    }
}

// 验证密码强度
export function checkPwd(s) {
    var lvl = 0
    if (s.match(/[0-9]/)) {
        lvl++
    }
    if (s.match(/[a-zA-Z]/)) {
        lvl++
    }
    if (s.match(/[^0-9a-zA-Z]/)) {
        lvl++
    }
    if (s.length < 6) {
        lvl--
    }
    return lvl
}

// 重写toast,只弹出一个
export function Toast() {
    const message = function () {
        ElMessage.closeAll()
        return ElMessage(...arguments)
    }

    for (let attribute in ElMessage) {
        message[attribute] = function () {
            ElMessage.closeAll()
            return ElMessage[attribute](...arguments)
        }
    }
    return message
}

// 文件下载函数
export function fileDownload(url, filename) {
    function getBlob(url, cb) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.responseType = 'blob';
        xhr.onload = function () {
            if (xhr.status === 200) {
                cb(xhr.response);
            }
        };
        xhr.send();
    }
    function saveAs(blob, filename) {
        if (window.navigator.msSaveOrOpenBlob) {
            navigator.msSaveBlob(blob, filename);
            return
        }
        var link = document.createElement('a');
        var body = document.querySelector('body');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;
        link.style.display = 'none';
        body.appendChild(link);
        link.click();
        body.removeChild(link);
        window.URL.revokeObjectURL(link.href);
    }
    getBlob(url, function (blob) {
        saveAs(blob, filename);
    })
}

export const taskStatus = (value) => {
    switch (value) {
        case -1: return "已回滚"
        case 0: return '进行中'
        case 1: return '提交成功'
        case 2: return '提交失败'
        case 3: return '回滚失败'
        default: return '未知'
    }
}

export const ocpcType = {
    22: '快应用付费',
    27: '快应用加桌',
    32: '快应用关键行为'
}

// 锚点-跳转到页面指定位置
export const anchorPoint = (id) => {
    document.querySelector(`#${id}`).scrollIntoView({ behavior: "smooth" })
}