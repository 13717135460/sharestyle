
export default {
    login: {
        url: '/api/auth/login',
        method: 'post',
        Type: 'application/json'
    },
    // 获取用户信息
    getUserInfoApi: {
        url: '/api/auth/user',
        method: 'get',
        Type: 'application/json'
    }
}
