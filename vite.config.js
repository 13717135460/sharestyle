import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import pxtorem from 'postcss-pxtorem'
import { resolve } from 'path'

function pathResolve(dir) {
    return resolve(__dirname, '.', dir)
}

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    css: {
        postcss: {
          plugins: [
            pxtorem({
              rootValue: 180, // 这里写设计稿的宽度/10即可，例如设计稿宽度是750px就写75
              // vant默认是37.5，如果是使用了vant的话可以像下面这样写
              // rootValue(res) {
              //   return res.file.indexOf("vant") !== -1 ? 37.5 : 75;
              // },
              propList: ['*'], // 需要转换的属性，默认转换所有属性
              selectorBlackList: [], // CSS选择器黑名单，防止部分选择器被转换
              exclude: /\/node_modules\//i, // 忽略包文件转换rem
            })
          ]
        }
    },
    resolve: {
        alias: {
            "@": pathResolve('src') // 设置别名
        },
        // extensions: ['.js', '.ts', '.json'] // 导入时想要省略的扩展名列表
    },
    server: {
        host: true, // 监听所有地址
        port: 8080, // 指定服务端口
        https: false, // 开启https协议
        hmr: {
            overlay: false // 关闭代码错误弹窗提示
        },
        proxy: {
            // '/api': {
            //     target: 'https://www.dongqiudi.com',
            //     changeOrigin: true,
            //     rewrite: (path) => path.replace(/^\/api/, ''),
            // },
        }
    },
    build: {
        // outDir: 'outPath' // 指定打包输出目录，默认dist
    }
})
